package com.epam.bookservice.Service;

import com.epam.bookservice.dto.BookDto;
import com.epam.bookservice.exception.BookNotFoundException;
import com.epam.bookservice.model.Book;
import com.epam.bookservice.repository.BookRepository;
import com.epam.bookservice.service.BookService;
import com.epam.bookservice.service.BookServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;


public class BookServiceTest {

  @InjectMocks
  BookService bookService = new BookServiceImpl();

  @Mock
  BookRepository bookRepository;

  @Mock
  ModelMapper modelMapper;

  @Before
  public void setup(){
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testSave(){
     BookDto bookDto = new BookDto("Dummy","Dummy","Dummy");
      bookService.save(bookDto);
      verify(bookRepository,times(1)).save(any());
  }

  @Test
  public void testdeleteById(){
    bookService.deleteById(1L);
    verify(bookRepository).deleteById(anyLong());
  }

  @Test(expected = BookNotFoundException.class)
  public void testfindAllBookNotFoundException(){
    //given
    List<Book> emptyBookList = Collections.emptyList();
    //when
    when(bookRepository.findAll()).thenReturn(emptyBookList);
    //then
    bookService.findAll();
    verify(bookRepository).findAll();
  }

  @Test
  public void testfindAll(){
    //given
    List<Book> bookList = new ArrayList<>();
    Book book = new Book("dummy","dummy","dummy");
    bookList.add(book);
    //when
    when(bookRepository.findAll()).thenReturn(bookList);
    //then
    assertTrue(bookService.findAll().contains(book));
  }

  @Test(expected = BookNotFoundException.class)
  public void testfindByidBookNotFoundException(){
    Optional<Book> emptyBookObject = Optional.empty();
    when(bookRepository.findById(1L)).thenReturn(emptyBookObject);
    bookService.findById(1L);
  }

  @Test
  public void testfindByid(){
    //given
    Book book = new Book("dummy","dummy","dummy");
    Optional<Book> bookObject = Optional.of(book);
    //when
    when(bookRepository.findById(1L)).thenReturn(bookObject);
    String author = bookService.findById(1L).getAuthor();
    //then
    assertEquals("dummy",author);
  }

  @Test(expected = BookNotFoundException.class)
  public void testUpdateBookNotFoundException(){
    //given
    BookDto bookDto = new BookDto(1L,"dummy","dummy","dummy");
    Optional<Book> emptyBookObject = Optional.empty();
    //when
    Book book = new Book(1L,"dummy","dummy","dummy");
    when(modelMapper.map(bookDto,Book.class)).thenReturn(book);
    when(bookRepository.findById(bookDto.getId())).thenReturn(emptyBookObject);
    //then
    bookService.update(bookDto);
  }

  @Test
  public void testUpdate(){
    //given
    BookDto bookDto = new BookDto(1L,"dummy","dummy","dummy");
    Book book = new Book(1L,"dummy","dummy","dummy");
    Optional<Book> bookObject = Optional.of(book);
    //when
    when(modelMapper.map(bookDto,Book.class)).thenReturn(book);
    when(bookRepository.findById(bookDto.getId())).thenReturn(bookObject);
    when(bookRepository.save(book)).thenReturn(book);
    //then
    Book updatedBook = bookService.update(bookDto);
    assertEquals(book,updatedBook);
  }
}


