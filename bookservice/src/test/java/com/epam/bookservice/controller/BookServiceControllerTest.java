package com.epam.bookservice.controller;

import com.epam.bookservice.model.Book;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;

public class BookServiceControllerTest  extends AbstractTest{

  @Override
  @Before
  public void setUp() {
    super.setUp();
  }

  @Test
  public void createBook() throws Exception {
    String uri = "/books";
    Book book = new Book();
    book.setId(1l);
    book.setAuthor("dummy");
    book.setCategory("dummy");
    book.setDescription("dummy");

    String inputJson = super.mapToJson(book);
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
      .contentType(MediaType.APPLICATION_JSON_VALUE)
      .content(inputJson)).andReturn();

    int status = mvcResult.getResponse().getStatus();
    assertEquals(201, status);
  }

  @Test
  public void deleteProduct() throws Exception {
    String uri = "/books/1";
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
    int status = mvcResult.getResponse().getStatus();
    assertEquals(200, status);
    String content = mvcResult.getResponse().getContentAsString();
  }

  @Test
  public void updateBook() throws Exception {
    String uri = "/books";
    Book book = new Book();
    book.setId(1l);
    book.setAuthor("dummy1");
    book.setCategory("dummy");
    book.setDescription("dummy");
    String inputJson = super.mapToJson(book);
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
      .contentType(MediaType.APPLICATION_JSON_VALUE)
      .content(inputJson)).andReturn();

    int status = mvcResult.getResponse().getStatus();
    assertEquals(200, status);
  }

  @Test
  public void getBooksList() throws Exception {
    String uri = "/books";
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
      .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

    int status = mvcResult.getResponse().getStatus();
    assertEquals(200, status);
 }
}
