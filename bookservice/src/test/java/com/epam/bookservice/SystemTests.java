package com.epam.bookservice;

import com.epam.bookservice.dto.BookDto;
import com.epam.bookservice.model.Book;
import com.epam.bookservice.util.APIResponse;
import org.junit.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class SystemTests {

  @Test
  public void testCreate() {
    RestTemplate restTemplate = new RestTemplate();
    BookDto bookDto = new BookDto("Pranav", "dummy", "dummy");
    String url = "http://localhost:9090/books";

    HttpEntity<BookDto> httpEntity = getHttpEntity(bookDto);

    ParameterizedTypeReference<APIResponse<Book>> myBean =
      new ParameterizedTypeReference<APIResponse<Book>>() {
      };

    ResponseEntity<APIResponse<Book>> book = restTemplate.exchange(url, HttpMethod.POST, httpEntity, myBean);

    assertEquals("Pranav", book.getBody().getData().getAuthor());
    restTemplate.delete(url + "/" + book.getBody().getData().getId());

  }

  @Test
  public void testGet() {
    RestTemplate restTemplate = new RestTemplate();
    String url = "http://localhost:9090/books";

    ParameterizedTypeReference<APIResponse<List<Book>>> myBean =
      new ParameterizedTypeReference<APIResponse<List<Book>>>() {
      };

    ResponseEntity<APIResponse<List<Book>>> book = restTemplate.exchange(url, HttpMethod.GET, null, myBean);

    assertFalse(book.getBody().getData().isEmpty());
  }

  @Test
  public void testUpdate() {
    RestTemplate restTemplate = new RestTemplate();
    BookDto bookDto = new BookDto("Pranav", "dummy", "dummy");
    String url = "http://localhost:9090/books";


    HttpEntity<BookDto> httpEntity = getHttpEntity(bookDto);

    ParameterizedTypeReference<APIResponse<Book>> myBean =
      new ParameterizedTypeReference<APIResponse<Book>>() {
      };

    ResponseEntity<APIResponse<Book>> book = restTemplate.exchange(url, HttpMethod.POST, httpEntity, myBean);

    BookDto bookDtoUpdate = new BookDto(book.getBody().getData().getId(), "Pranav1", "dummy", "dummy");

    HttpEntity<BookDto> httpEntityUpdate = getHttpEntity(bookDtoUpdate);
    ResponseEntity<APIResponse<Book>> bookupdate = restTemplate.exchange(url, HttpMethod.POST, httpEntityUpdate, myBean);

    assertEquals("Pranav1", bookupdate.getBody().getData().getAuthor());

  }


  private HttpEntity<BookDto> getHttpEntity(BookDto bookDto) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
    return new HttpEntity<>(bookDto, headers);
  }

}
