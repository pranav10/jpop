package com.epam.bookservice.controller;


import com.epam.bookservice.dto.BookDto;
import com.epam.bookservice.service.BookService;
import com.epam.bookservice.util.APIResponse;
import com.epam.bookservice.util.APIStatus;
import com.epam.bookservice.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class BookController {

  @Autowired
  BookService bookService;

  @Autowired
  protected ResponseUtil responseUtil;


  @GetMapping("/books")
  ResponseEntity<APIResponse> read(){
    return responseUtil.successResponse(bookService.findAll());
  }

  @GetMapping("/books/{id}")
  ResponseEntity<APIResponse> findById(@PathVariable Long id){
    return responseUtil.successResponse(bookService.findById(id));
  }

  @PostMapping("/books")
  ResponseEntity<APIResponse> create(@Valid @RequestBody BookDto bookDto){
    return responseUtil.buildResponse(APIStatus.CREATED,bookService.save(bookDto),HttpStatus.CREATED);
  }

  @DeleteMapping("/books/{id}")
  void delete(@PathVariable Long id){
     bookService.deleteById(id);
  }

  @PutMapping("/books")
  ResponseEntity<APIResponse> update(@RequestBody BookDto bookDto){
    return responseUtil.successResponse(bookService.update(bookDto));
  }

}
