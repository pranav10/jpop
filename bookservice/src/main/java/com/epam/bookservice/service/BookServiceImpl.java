package com.epam.bookservice.service;

import com.epam.bookservice.dto.BookDto;
import com.epam.bookservice.exception.BookNotFoundException;
import com.epam.bookservice.model.Book;
import com.epam.bookservice.repository.BookRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

  @Autowired
  BookRepository bookRepository;

  @Autowired
  private ModelMapper modelMapper;

  @Override
  public Book findById(Long id) {
           return bookRepository.findById(id).orElseThrow(() -> new BookNotFoundException("Book not Found with the given id"));
  }

  @Override
  public List<Book> findAll() {
    List<Book> book = bookRepository.findAll();
    if(book.isEmpty()){
      throw  new BookNotFoundException("Book Not Found");
    }
    return book;
  }

  @Override
  public void deleteById(Long id) {
     bookRepository.deleteById(id);
  }

  @Override
  public Book save(BookDto bookDto) {
    Book book = convertToEntity(bookDto);
    return bookRepository.save(book);
  }

  @Override
  public Book update(BookDto bookDto) {
    Book book = convertToEntity(bookDto);
    if (bookRepository.findById(book.getId()).isPresent()) {
    return bookRepository.save(book);
  } else {
    throw new BookNotFoundException("No Book found with book id"+book.getId());
  }
  }

  public Book convertToEntity(BookDto bookDto) {
    Book book = modelMapper.map(bookDto,Book.class);
    return book;
  }

  public BookDto convertToDto(Book book){
    BookDto bookDto = modelMapper.map(book,BookDto.class);
    return bookDto;
  }
}
