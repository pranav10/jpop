package com.epam.bookservice.service;

import com.epam.bookservice.dto.BookDto;
import com.epam.bookservice.model.Book;

import java.util.List;

public interface BookService  {
Book findById(Long Id);
List<Book> findAll();
void deleteById(Long id);
Book save(BookDto bookDto);
Book update(BookDto bookDto);
}
