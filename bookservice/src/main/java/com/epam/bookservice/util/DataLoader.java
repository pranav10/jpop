package com.epam.bookservice.util;

import com.epam.bookservice.model.Book;
import com.epam.bookservice.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataLoader {

  private BookRepository bookRepository;

  @Autowired
  public DataLoader(BookRepository bookRepository) {
    this.bookRepository = bookRepository;
    LoadUsers();
  }

  private void LoadUsers() {
    bookRepository.save(new Book("lala", "lala", "lala"));
  }
}
