package com.epam.libraryService;

import com.epam.libraryService.controller.LibraryController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LibraryServiceApplicationTests {

  @Autowired
  LibraryController libraryController;

	@Test
	public void contextLoads() {
    Assert.assertNotNull(libraryController);
	}

}
