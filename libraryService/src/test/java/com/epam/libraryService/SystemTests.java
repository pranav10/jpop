package com.epam.libraryService;

import com.epam.libraryService.model.Book;
import com.epam.libraryService.model.BookDto;
import com.epam.libraryService.model.User;
import com.epam.libraryService.model.UserDto;
import com.epam.libraryService.util.APIResponse;
import org.junit.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SystemTests {


  @Test
  public void testgetUserByUserId(){
    RestTemplate restTemplate = new RestTemplate();
    String url = "http://localhost:8090/lib/users/1";

    ParameterizedTypeReference<APIResponse<User>> myBean =
      new ParameterizedTypeReference<APIResponse<User>>() {
      };

    ResponseEntity<APIResponse<User>> useresponse = restTemplate.exchange(url, HttpMethod.GET, null, myBean);

    assertEquals("Pranav",useresponse.getBody().getData().getName());
  }

  @Test
  public void testgetAllUser(){
    RestTemplate restTemplate = new RestTemplate();
    String url = "http://localhost:8090/lib/users";

    ParameterizedTypeReference<APIResponse<List<User>>> myBean =
      new ParameterizedTypeReference<APIResponse<List<User>>>() {
      };

    ResponseEntity<APIResponse<List<User>>> getreponse = restTemplate.exchange(url, HttpMethod.GET, null, myBean);

     assertFalse(getreponse.getBody().getData().isEmpty());
  }



  @Test
  public  void testcreateUser(){
    RestTemplate restTemplate = new RestTemplate();
    String url = "http://localhost:8090/lib/users";

    UserDto userDto = new UserDto();
    userDto.setName("Pranav1");
    userDto.setAddress("Pune");

    HttpEntity<UserDto> httpEntity = getHttpEntity(userDto);


    ParameterizedTypeReference<APIResponse<User>> myBean =
      new ParameterizedTypeReference<APIResponse<User>>() {
      };

    ResponseEntity<APIResponse<User>> postresponse = restTemplate.exchange(url, HttpMethod.POST, httpEntity, myBean);

    assertEquals("201 CREATED",postresponse.getStatusCode().toString());
  }


  @Test
  public void testgetBookByBookId(){
    RestTemplate restTemplate = new RestTemplate();
    String url = "http://localhost:8090/lib/books/1";

    ParameterizedTypeReference<APIResponse<Book>> myBean =
      new ParameterizedTypeReference<APIResponse<Book>>() {
      };

    ResponseEntity<APIResponse<Book>> useresponse = restTemplate.exchange(url, HttpMethod.GET, null, myBean);

    assertEquals("Pranav",useresponse.getBody().getData().getAuthor());
  }


  @Test
  public  void testcreateBook(){
    RestTemplate restTemplate = new RestTemplate();
    String url = "http://localhost:8090/lib/books";

    BookDto bookDto = new BookDto();
    bookDto.setAuthor("Pranav1");
    bookDto.setCategory("Dummy");
    bookDto.setDescription("dummyDescription");

    HttpEntity<BookDto> httpEntity = getHttpEntity(bookDto);


    ParameterizedTypeReference<APIResponse<Book>> myBean =
      new ParameterizedTypeReference<APIResponse<Book>>() {
      };

    ResponseEntity<APIResponse<Book>> postresponse = restTemplate.exchange(url, HttpMethod.POST, httpEntity, myBean);

    assertEquals("201 CREATED",postresponse.getStatusCode().toString());
  }

  @Test
  public void testgetAllBook(){
    RestTemplate restTemplate = new RestTemplate();
    String url = "http://localhost:8090/lib/books";

    ParameterizedTypeReference<APIResponse<List<Book>>> myBean =
      new ParameterizedTypeReference<APIResponse<List<Book>>>() {
      };

    ResponseEntity<APIResponse<List<Book>>> getreponse = restTemplate.exchange(url, HttpMethod.GET, null, myBean);

    assertFalse(getreponse.getBody().getData().isEmpty());
  }
  private HttpEntity<UserDto> getHttpEntity(UserDto userDto) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
    return new HttpEntity<>(userDto, headers);
  }

  private HttpEntity<BookDto> getHttpEntity(BookDto bookDto) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
    return new HttpEntity<>(bookDto, headers);
  }
}
