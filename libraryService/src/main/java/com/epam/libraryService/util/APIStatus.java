package com.epam.libraryService.util;

public enum APIStatus {
  // Common status
  OK(200, null),
  CREATED(201,null),
  NOTFOUND(404,null),
  INTERNALSERVERERROR(500,null);
  private final int code;
  private final String description;

  private APIStatus(int s, String v) {
    code = s;
    description = v;
  }

  public int getCode() {
    return code;
  }

  public String getDescription() {
    return description;
  }
}
