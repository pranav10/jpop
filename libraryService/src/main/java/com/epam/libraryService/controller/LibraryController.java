package com.epam.libraryService.controller;


import com.epam.libraryService.model.Book;
import com.epam.libraryService.model.BookDto;
import com.epam.libraryService.model.User;
import com.epam.libraryService.model.UserDto;
import com.epam.libraryService.proxy.BookServiceProxy;
import com.epam.libraryService.proxy.UserServiceProxy;
import com.epam.libraryService.util.APIResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/lib")
public class LibraryController  {

  private Logger logger = LoggerFactory.getLogger(this.getClass());

  @Autowired
  BookServiceProxy bookServiceProxy;

  @Autowired
  UserServiceProxy userServiceProxy;

  @GetMapping("/users/{id}")
  public ResponseEntity<APIResponse<User>> getUserByUserId(@PathVariable Long id){
    ResponseEntity<APIResponse<User>> useresponse =  userServiceProxy.getUserByUserId(id);
    logger.info("{}",useresponse);

    return useresponse;
  }

  @GetMapping("/users")
  public ResponseEntity<APIResponse<List<User>>> getAllUser(){
    ResponseEntity<APIResponse<List<User>>> getreponse = userServiceProxy.getAllUser();
    logger.info("{}",getreponse);
    return getreponse;
  }

  @DeleteMapping("/users/{id}")
  public ResponseEntity<APIResponse<User>> deleteUserByUserId(@PathVariable Long id){
    ResponseEntity<APIResponse<User>> deleteresponse = userServiceProxy.deleteUserByUserId(id);
    logger.info("{}",deleteresponse);
    return deleteresponse;
  }

  @PostMapping("/users")
  public ResponseEntity<APIResponse<User>> createUser(@RequestBody UserDto userDto){
    ResponseEntity<APIResponse<User>> postresponse = userServiceProxy.createUser(userDto);
    logger.info("{}",postresponse);
    return postresponse;
  }

  @PutMapping("/users")
  public ResponseEntity<APIResponse<User>> updateUser(@RequestBody UserDto userDto){
    ResponseEntity<APIResponse<User>> putresponse = userServiceProxy.updateUser(userDto);
    logger.info("{}",putresponse);
    return putresponse;
  }

  @GetMapping("/books/{id}")
  public ResponseEntity<APIResponse<Book>> getBookByBookId(@PathVariable Long id){
    ResponseEntity<APIResponse<Book>> bookresponse = bookServiceProxy.getBookByBookId(id);
    logger.info("{}",bookresponse);
    return bookresponse;
  }

  @GetMapping("/books")
  public ResponseEntity<APIResponse<List<Book>>> getAllBook(){
    ResponseEntity<APIResponse<List<Book>>> getreponse = bookServiceProxy.getAllBook();
    logger.info("{}",getreponse);
    return getreponse;
  }

  @DeleteMapping("/books/{id}")
  public void deleteBookByBookId(@PathVariable Long id){
    bookServiceProxy.deleteBookByBookId(id);

  }

  @PostMapping("/books")
  public ResponseEntity<APIResponse<Book>> createBook(@RequestBody BookDto bookDto){
    ResponseEntity<APIResponse<Book>> postresponse = bookServiceProxy.createBook(bookDto);
    logger.info("{}",postresponse);
    return postresponse;
  }


  @PutMapping("/books")
  public ResponseEntity<APIResponse<Book>> updateBook(@RequestBody BookDto bookDto){
    ResponseEntity<APIResponse<Book>> putresponse = bookServiceProxy.updateBook(bookDto);
    logger.info("{}",putresponse);
    return putresponse;
  }
  private HttpEntity<UserDto> getHttpEntity(UserDto userDto) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
    return new HttpEntity<>(userDto, headers);
  }

  private HttpEntity<BookDto> getHttpEntity(BookDto bookDto) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
    return new HttpEntity<>(bookDto, headers);
  }


}

