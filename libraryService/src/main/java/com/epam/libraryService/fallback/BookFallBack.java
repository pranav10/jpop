package com.epam.libraryService.fallback;

import com.epam.libraryService.model.Book;
import com.epam.libraryService.model.BookDto;
import com.epam.libraryService.proxy.BookServiceProxy;
import com.epam.libraryService.util.APIResponse;
import com.epam.libraryService.util.APIStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BookFallBack implements BookServiceProxy {
  @Override
  public ResponseEntity<APIResponse<List<Book>>> getAllBook() {
    Book book = new Book();
    book.setAuthor("Book Not Available");
    book.setCategory("Book Not Available");
    book.setDescription("Book Not Available");
    List<Book> books = new ArrayList<Book>();
    books.add(book);
    APIResponse apiResponse = new APIResponse(APIStatus.INTERNALSERVERERROR , books);
    return (ResponseEntity.accepted().body(apiResponse));
  }

  @Override
  public ResponseEntity<APIResponse<Book>> getBookByBookId(Long id) {
    Book book = new Book();
    book.setId(id);
    book.setAuthor("Book Not Available");
    book.setCategory("Book Not Available");
    book.setDescription("Book Not Available");
    APIResponse apiResponse = new APIResponse(APIStatus.INTERNALSERVERERROR , book);
    return  ResponseEntity.accepted().body(apiResponse);
  }

  @Override
  public ResponseEntity<APIResponse<Book>> createBook(BookDto bookDto) {
    Book book = new Book();
    book.setId(bookDto.getId());
    book.setAuthor("Book Not Available");
    book.setCategory("Book Not Available");
    book.setDescription("Book Not Available");
    APIResponse apiResponse = new APIResponse(APIStatus.INTERNALSERVERERROR , book);
    return  ResponseEntity.accepted().body(apiResponse);
  }

  @Override
  public void deleteBookByBookId(Long id) {
  }


  @Override
  public ResponseEntity<APIResponse<Book>> updateBook(BookDto bookDto) {
    Book book = new Book();
    book.setId(bookDto.getId());
    book.setAuthor("Book Not Available");
    book.setCategory("Book Not Available");
    book.setDescription("Book Not Available");
    APIResponse apiResponse = new APIResponse(APIStatus.INTERNALSERVERERROR , book);
    return  ResponseEntity.accepted().body(apiResponse);
  }
}
