package com.epam.libraryService.fallback;

import com.epam.libraryService.model.User;
import com.epam.libraryService.model.UserDto;
import com.epam.libraryService.proxy.UserServiceProxy;
import com.epam.libraryService.util.APIResponse;
import com.epam.libraryService.util.APIStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserFallBack  implements UserServiceProxy {

  @Override
  public ResponseEntity<APIResponse<List<User>>> getAllUser() {
    User user = new User();
    user.setName("User Not Available");
    user.setAddress("Address Not Available");
    List<User> users = new ArrayList<User>();
    users.add(user);
    APIResponse apiResponse = new APIResponse(APIStatus.INTERNALSERVERERROR , users);
    return (ResponseEntity.accepted().body(apiResponse));
  }

  @Override
  public ResponseEntity<APIResponse<User>> getUserByUserId(Long id) {
    User user = new User();
    user.setId(id);
    user.setName("User not available");
    user.setAddress("Address Not available");
    APIResponse apiResponse = new APIResponse(APIStatus.INTERNALSERVERERROR , user);
    return  ResponseEntity.accepted().body(apiResponse);
  }

  @Override
  public ResponseEntity<APIResponse<User>> createUser(UserDto userDto) {
    User user = new User();
    APIResponse apiResponse = new APIResponse(APIStatus.INTERNALSERVERERROR , user);
    return  ResponseEntity.accepted().body(apiResponse);
  }

  @Override
  public ResponseEntity<APIResponse<User>> deleteUserByUserId(Long id) {
    User user = new User();
    user.setId(id);
    user.setName("User not available");
    user.setAddress("Address Not available");
    APIResponse apiResponse = new APIResponse(APIStatus.INTERNALSERVERERROR , user);
    return  ResponseEntity.accepted().body(apiResponse);
  }

  @Override
  public ResponseEntity<APIResponse<User>> updateUser(UserDto userDto) {
    User user = new User();
    user.setId(userDto.getId());
    user.setName("User not available");
    user.setAddress("Address Not available");
    APIResponse apiResponse = new APIResponse(APIStatus.INTERNALSERVERERROR , user);
    return  ResponseEntity.accepted().body(apiResponse);
  }
}
