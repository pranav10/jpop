package com.epam.libraryService.proxy;


import com.epam.libraryService.fallback.UserFallBack;
import com.epam.libraryService.model.Book;
import com.epam.libraryService.model.BookDto;
import com.epam.libraryService.model.User;
import com.epam.libraryService.model.UserDto;
import com.epam.libraryService.util.APIResponse;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name="user-service", fallback = UserFallBack.class)
@RibbonClient(name="user-service")
public interface UserServiceProxy {
  @GetMapping("/users")
  public ResponseEntity<APIResponse<List<User>>> getAllUser();
  @GetMapping("/users/{id}")
  public ResponseEntity<APIResponse<User>> getUserByUserId(@PathVariable("id")Long id);
  @PostMapping("/users")
  public ResponseEntity<APIResponse<User>> createUser(@RequestBody UserDto userDto);
  @DeleteMapping("/users/{id}")
  public ResponseEntity<APIResponse<User>> deleteUserByUserId(@PathVariable("id")Long id);
  @PutMapping("/users")
  ResponseEntity<APIResponse<User>> updateUser(@RequestBody UserDto userDto);
}
