package com.epam.libraryService.proxy;

import com.epam.libraryService.fallback.BookFallBack;
import com.epam.libraryService.fallback.UserFallBack;
import com.epam.libraryService.model.Book;
import com.epam.libraryService.model.BookDto;
import com.epam.libraryService.util.APIResponse;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name="netflix-zuul-api-gateway-server" , fallback = BookFallBack.class)
@RibbonClient(name="book-service")
public interface BookServiceProxy {
  @GetMapping("book-service/books")
  public ResponseEntity<APIResponse<List<Book>>> getAllBook();
  @GetMapping("book-service/books/{id}")
  public ResponseEntity<APIResponse<Book>> getBookByBookId(@PathVariable("id")Long id);
  @PostMapping("book-service/books")
  public ResponseEntity<APIResponse<Book>> createBook(@RequestBody BookDto bookDto);
  @DeleteMapping("book-service/books/{id}")
  public void deleteBookByBookId(@PathVariable("id")Long id);
  @PutMapping("book-service/books")
  ResponseEntity<APIResponse<Book>> updateBook(@RequestBody BookDto bookDto);
}
