package com.epam.libraryService.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class User {

  private  Long id;
  private String name;
  private String Address;

  public User() {
  }

  public User(String name, String address) {
    this.name = name;
    Address = address;
  }

  public User(Long id, String name, String address) {
    this.id = id;
    this.name = name;
    Address = address;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return Address;
  }

  public void setAddress(String address) {
    Address = address;
  }
}
