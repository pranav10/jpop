package com.epam.libraryService.model;

import javax.validation.constraints.NotBlank;

public class BookDto {
  private  Long id;

  @NotBlank(message = "Author cant be null or blank")
  private String author;

  @NotBlank(message = "Category cant be null or blank")
  private String category;

  @NotBlank(message = "Description cant be null or blank")
  private String description;

  public BookDto() {
  }

  public BookDto(Long id,  String author,  String category, String description) {
    this.id = id;
    this.author = author;
    this.category = category;
    this.description = description;
  }

  public BookDto(String author, String category, String description) {
    this.author = author;
    this.category = category;
    this.description = description;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
