package com.epam.UserService;

import com.epam.UserService.dto.UserDto;
import com.epam.UserService.model.User;
import com.epam.UserService.util.APIResponse;
import org.junit.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class SystemTests {

  @Test
  public void testCreate() {
    RestTemplate restTemplate = new RestTemplate();
    UserDto userDto = new UserDto(1L, "Pranav", "dummy");
    String url = "http://localhost:9090/users";

    HttpEntity<UserDto> httpEntity = getHttpEntity(userDto);

    ParameterizedTypeReference<APIResponse<User>> myBean =
      new ParameterizedTypeReference<APIResponse<User>>() {
      };

    ResponseEntity<APIResponse<User>> user = restTemplate.exchange(url, HttpMethod.POST, httpEntity, myBean);

    assertEquals("Pranav", user.getBody().getData().getName());
    restTemplate.delete(url + "/" + user.getBody().getData().getId());

  }

  @Test
  public void testGet() {
    RestTemplate restTemplate = new RestTemplate();
    String url = "http://localhost:9090/users";

    ParameterizedTypeReference<APIResponse<List<User>>> myBean =
      new ParameterizedTypeReference<APIResponse<List<User>>>() {
      };

    ResponseEntity<APIResponse<List<User>>> book = restTemplate.exchange(url, HttpMethod.GET, null, myBean);

    assertFalse(book.getBody().getData().isEmpty());
  }

  @Test
  public void testUpdate() {
    RestTemplate restTemplate = new RestTemplate();
    UserDto userDto = new UserDto(1L, "Pranav", "dummy");
    String url = "http://localhost:9090/books";


    HttpEntity<UserDto> httpEntity = getHttpEntity(userDto);

    ParameterizedTypeReference<APIResponse<User>> myBean =
      new ParameterizedTypeReference<APIResponse<User>>() {
      };

    ResponseEntity<APIResponse<User>> book = restTemplate.exchange(url, HttpMethod.POST, httpEntity, myBean);

    UserDto bookDtoUpdate = new UserDto(book.getBody().getData().getId(), "Pranav1", "dummy");

    HttpEntity<UserDto> httpEntityUpdate = getHttpEntity(bookDtoUpdate);
    ResponseEntity<APIResponse<User>> bookupdate = restTemplate.exchange(url, HttpMethod.PUT, httpEntityUpdate, myBean);

    assertEquals("Pranav1", bookupdate.getBody().getData().getName());

  }


  private HttpEntity<UserDto> getHttpEntity(UserDto userDto) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
    return new HttpEntity<>(userDto, headers);
  }

}
