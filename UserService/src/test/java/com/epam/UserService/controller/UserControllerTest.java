package com.epam.UserService.controller;

import com.epam.UserService.dto.UserDto;
import com.epam.UserService.model.User;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;

public class UserControllerTest extends AbstractTest {

  @Override
  @Before
  public void setUp() {
    super.setUp();
  }

  @Test
  public void createUser() throws Exception {
    String uri = "/users";
    User user = new User();
    user.setId(1l);
    user.setName("dummy");
    user.setAddress("dummy");

    String inputJson = super.mapToJson(user);
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
      .contentType(MediaType.APPLICATION_JSON_VALUE)
      .content(inputJson)).andReturn();

    int status = mvcResult.getResponse().getStatus();
    assertEquals(201, status);
  }

  @Test
  public void deleteUser() throws Exception {
    String uri = "/users/1";
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
    int status = mvcResult.getResponse().getStatus();
    assertEquals(200, status);
    String content = mvcResult.getResponse().getContentAsString();
  }

  @Test
  public void updateUser() throws Exception {
    String uri = "/users";
    User user = new User();
    user.setId(1l);
    user.setName("dummy1");
    user.setAddress("dummy");
    String inputJson = super.mapToJson(user);
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
      .contentType(MediaType.APPLICATION_JSON_VALUE)
      .content(inputJson)).andReturn();

    int status = mvcResult.getResponse().getStatus();
    assertEquals(200, status);
  }

  @Test
  public void getUsersList() throws Exception {
    String uri = "/users";
    MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
      .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

    int status = mvcResult.getResponse().getStatus();
    assertEquals(200, status);
  }

}
