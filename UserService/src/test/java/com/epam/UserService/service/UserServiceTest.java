package com.epam.UserService.service;

import com.epam.UserService.dto.UserDto;
import com.epam.UserService.exception.UserNotFoundException;
import com.epam.UserService.model.User;
import com.epam.UserService.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserServiceTest {

  @InjectMocks
  UserService userService = new UserServiceImpl();

  @Mock
  UserRepository userRepository;

  @Mock
  ModelMapper modelMapper;

  @Before
  public void setup(){
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void testSave(){
    UserDto bookDto = new UserDto(1L,"Dummy","Dummy");
    userService.save(bookDto);
    verify(userRepository,times(1)).save(any());
  }

  @Test
  public void testdeleteById(){
    userService.deleteById(1L);
    verify(userRepository).deleteById(anyLong());
  }

  @Test(expected = UserNotFoundException.class)
  public void testfindAllBookNotFoundException(){
    //given
    List<User> emptyUserList = Collections.emptyList();
    //when
    when(userRepository.findAll()).thenReturn(emptyUserList);
    //then
    userService.findAll();
    verify(userRepository).findAll();
  }

  @Test
  public void testfindAll(){
    //given
    List<User> userList = new ArrayList<>();
    User user = new User("dummy","dummy");
    userList.add(user);
    //when
    when(userRepository.findAll()).thenReturn(userList);
    //then
    assertTrue(userService.findAll().contains(user));
  }

  @Test(expected = UserNotFoundException.class)
  public void testfindByidBookNotFoundException(){
    Optional<User> emptyUserObject = Optional.empty();
    when(userRepository.findById(1L)).thenReturn(emptyUserObject);
    userService.findById(1L);
  }

  @Test
  public void testfindByid(){
    //given
    User user = new User("dummy","dummy");
    Optional<User> userObject = Optional.of(user);
    //when
    when(userRepository.findById(1L)).thenReturn(userObject);
    String author = userService.findById(1L).getName();
    //then
    assertEquals("dummy",author);
  }

  @Test(expected = UserNotFoundException.class)
  public void testUpdateBookNotFoundException(){
    //given
    UserDto userDto = new UserDto(1L,"dummy","dummy");
    Optional<User> emptyUserObject = Optional.empty();
    //when
    User book = new User(1L,"dummy","dummy");
    when(modelMapper.map(userDto,User.class)).thenReturn(book);
    when(userRepository.findById(userDto.getId())).thenReturn(emptyUserObject);
    //then
    userService.update(userDto);
  }

  @Test
  public void testUpdate(){
    //given
    UserDto bookDto = new UserDto(1L,"dummy","dummy");
    User user = new User(1L,"dummy","dummy");
    Optional<User> userObject = Optional.of(user);
    //when
    when(modelMapper.map(bookDto,User.class)).thenReturn(user);
    when(userRepository.findById(bookDto.getId())).thenReturn(userObject);
    when(userRepository.save(user)).thenReturn(user);
    //then
    User updatedBook = userService.update(bookDto);
    assertEquals(user,updatedBook);
  }

}
