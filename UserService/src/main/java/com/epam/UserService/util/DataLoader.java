package com.epam.UserService.util;

import com.epam.UserService.model.User;
import com.epam.UserService.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataLoader {

  private UserRepository bookRepository;

  @Autowired
  public DataLoader(UserRepository bookRepository) {
    this.bookRepository = bookRepository;
    LoadUsers();
  }

  private void LoadUsers() {
    bookRepository.save(new User( "Pranav", "Pune"));
  }
}
