package com.epam.UserService.controller;

import com.epam.UserService.dto.UserDto;
import com.epam.UserService.service.UserService;
import com.epam.UserService.util.APIResponse;
import com.epam.UserService.util.APIStatus;
import com.epam.UserService.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class UserController {

  @Autowired
  UserService userService;

  @Autowired
  protected ResponseUtil responseUtil;

  @GetMapping("/users")
  ResponseEntity<APIResponse> read(){
    return responseUtil.successResponse(userService.findAll());
  }

  @GetMapping("/users/{id}")
  ResponseEntity<APIResponse> findById(@PathVariable Long id){
    return responseUtil.successResponse(userService.findById(id));
  }

  @PostMapping("/users")
  ResponseEntity<APIResponse> create(@Valid @RequestBody UserDto userDto){
    return responseUtil.buildResponse(APIStatus.CREATED, userService.save(userDto),HttpStatus.CREATED);
  }

  @DeleteMapping("/users/{id}")
  void delete(@PathVariable Long id){
    userService.deleteById(id);
  }

  @PutMapping("/users")
  ResponseEntity<APIResponse> update(@RequestBody UserDto userDto){
    return responseUtil.successResponse(userService.update(userDto));
  }
}
