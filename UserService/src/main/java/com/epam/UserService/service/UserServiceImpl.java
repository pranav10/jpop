package com.epam.UserService.service;

import com.epam.UserService.exception.UserNotFoundException;
import com.epam.UserService.dto.UserDto;
import com.epam.UserService.model.User;
import com.epam.UserService.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

  @Autowired
  UserRepository userRepository;

  @Autowired
  private ModelMapper modelMapper;

  @Override
  public User findById(Long Id) {
    return userRepository.findById(Id).orElseThrow(() -> new UserNotFoundException("User not Found with the given id"));
  }

  @Override
  public List<User> findAll() {
    List<User> user = userRepository.findAll();
    if(user.isEmpty()){
      throw  new UserNotFoundException("User Not Found");
    }
    return user;
  }

  @Override
  public void deleteById(Long id) {
    userRepository.deleteById(id);
  }

  @Override
  public User save(UserDto userDto) {
    User user = convertToEntity(userDto);
    return userRepository.save(user);
  }

  @Override
  public User update(UserDto userDto) {
    User user = convertToEntity(userDto);
    if (userRepository.findById(user.getId()).isPresent()) {
      return userRepository.save(user);
    } else {
      throw new UserNotFoundException("No Book found with book id"+user.getId());
    }
  }

  private User convertToEntity(UserDto userDto) {
    User user = modelMapper.map(userDto,User.class);
    return user;
  }
}
