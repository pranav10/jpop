package com.epam.UserService.service;

import com.epam.UserService.dto.UserDto;
import com.epam.UserService.model.User;

import java.util.List;

public interface UserService {
  User findById(Long Id);
  List<User> findAll();
  void deleteById(Long id);
  User save(UserDto user);
  User update(UserDto user);
}
