package com.epam.UserService.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.util.Date;

@ApiModel(description = "All information about the user")
public class UserDto {
  private Long id;

  @NotBlank(message = "Name  can not be null")
  @ApiModelProperty(notes="Address  can not be null")
  private String name;

  @NotBlank(message = "Address can not be null")
  @ApiModelProperty(notes="Address can not be null")
  private String Address;

  public UserDto() {
  }

  public UserDto(Long id, String name, String address) {
    this.id = id;
    this.name = name;
    Address = address;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return Address;
  }

  public void setAddress(String address) {
    Address = address;
  }
}
