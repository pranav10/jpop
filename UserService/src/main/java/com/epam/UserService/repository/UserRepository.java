package com.epam.UserService.repository;

import com.epam.UserService.dto.UserDto;
import com.epam.UserService.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
}
